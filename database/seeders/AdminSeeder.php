<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin1 = User::create([
            'name' => 'Administrador-Wec',
            'last_name'=> 'Colombia',
            'number_document'=> '123456789',
            'city'=>'Bogotá',
            'email' => 'adminwec@gmail.com',
            'number_phone'=>'123456789',
            'password' => Hash::make('MD-wec22'),
            'created_at' => now()->toDateString(),
            'updated_at' => now()->toDateString()
        ]);

        $admin1->assignRole('Administrador');

    }
}
