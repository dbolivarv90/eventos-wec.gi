<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{RegisterController,LoginController,DashboardController,EventController, UserController};
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register',  [RegisterController::class, 'index'])->name('register.index');
Route::post('/register', [RegisterController::class, 'store'])->name('register.store');
Route::get('/',[LoginController::class, 'index'])->name('login');
Route::post('/login',[LoginController::class, 'store'])->name('login.store');
Route::get('/loginadmin',[LoginController::class, 'admin'])->name('login.admin');
Route::post('/loginadmin',[LoginController::class, 'storeAdmin'])->name('login.adminstore');

Route::middleware(['auth'])->group(function () {
    Route::get('/lobby',[LoginController::class, 'lobby'])->name('login.lobby');
    Route::get('perfil/{id}/edit',[LoginController::class, 'edit'])->name('login.edit');
    Route::put('{usuario}/update',[LoginController::class, 'update'])->name('login.update');
    Route::get('{id}/event-register',[LoginController::class, 'eventregister'])->name('login.eventregister');
    Route::get('/logout',[LoginController::class, 'logout'])->name('login.logout');
    Route::get('/dashboard',[DashboardController::class, 'index'])->name('dashboard.index');
    Route::resources([
        'events' => EventController::class,
        'users' => UserController::class,
    ]);

    Route::post('/events/delete', [EventController::class, 'deleteEvent'])->name('events.delete');

    Route::get('/allEvents',[EventController::class, 'allEvents'])->name('events.allEvents'); 
    Route::get('/report/{id}',[EventController::class, 'report'])->name('events.report');   

    Route::post('file-import', [UserController::class, 'fileImport'])->name('file-import');
});