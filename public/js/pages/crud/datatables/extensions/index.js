$(document).ready(function(){

    var currentURL = $(location).attr("href");
    var url_route;

    if(currentURL.indexOf('events') > -1) {
        url_route = "events/delete";  
    } 

    $('.deleteAll').click(function(){
        var ids = [];

        $("input[type=checkbox]:checked").each(function(){
            ids.push($(this).val());
        }); 

        //Validate is selected rows
        if(ids.length <= 0) {    
            alert("¡Por favor selecciona al menos una columna!");    
        }  else {

            var url = url_route;
            var check_deleted = confirm("¿Seguro que deseas eliminar estos elementos?");   
                
            //Confirm delete rows 
            if(check_deleted == true){ 
                $.ajax({
                    url: url,
                    type: 'POST',                       
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: { ids: ids },               
                    success: function (data) {

                        setTimeout(() => {
                            toastr.success(data.message, data.title);
                        },500);

                        $('#kt_datatable_example_1').DataTable().draw();
                    }
                });
            }   
        }
    });
});  