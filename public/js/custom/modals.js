var KTModalActions = (function(){
	return {
		init: function () {
            document.querySelector(".form-modal-dismiss") ?
    			document.querySelector(".form-modal-dismiss").addEventListener("click", function (e) {
    			 	e.preventDefault(),
                    Swal.fire({
                        text: "¿Estás seguro de descartar el formulario?",
                        icon: "warning",
                        showCancelButton: !0,
                        buttonsStyling: !1,
                        confirmButtonText: "Sí, salir",
                        cancelButtonText: "No, volver",
                        customClass: { confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light" },
                    }).then(function (t) {

                        const params = new URLSearchParams(window.location.search)
                        let replace = '';

                        if(params.has('affiliate'))
                            replace = '?affiliate=1#create';
                        else
                            replace = '#create';

                        if(t.value)
                            window.location.href = window.location.href.replace(replace, ''); 
            
    						//document.querySelector('.modal.show [data-bs-dismiss]').click()

                    });
    			}) : null
		}
	}
})()


KTUtil.onDOMContentLoaded(function () {
    KTModalActions.init()
});