@extends('layouts.master')

@section('content')
<style>

.formulario{
 
 background-color: #EDEDED;
 color:#AEADB3;
 border-radius: 0px;
 font-size: 1.2rem;
 border: none;
 font-family: Montserrat;
 padding-left: 30px;
 

}

.form {
    display: flex;
    align-items: center;
    margin: 0 auto;
    margin-top: 5%;

}

 .contenido-registro{
    background-image: url('{{asset('imagenes/Fondo-1.png')}}'); 
    background-size:cover;
    background-position: center;
    padding-top: 40px;
    padding-bottom: 40px;
 }  
 
 .check-box{
     padding-top:10px;
     padding-bottom: 10px;
     padding-left:30px;
     font-family: Montserrat;
     font-size: 1.2rem;
     font-weight: 500;
     color: #4d4d4d;
 }
 .register-button{
     width: 100%;
     background-color:#0B91D6;
     padding-top:20px;
     padding-bottom:20px;
     color:#FFFFFF;
     font-family: Montserrat;
     font-size: 1.2rem;
     font-weight: 600;
     border: none;
     border-radius: 0px;
 }

</style>

<div class="row" style="padding-top:50px; padding-bottom:50px;">
    <div class="col-md-1"></div>
    <div class="col-md-3"><img src="{{ asset('imagenes/logo-color.png') }}" alt="logo" width="250px"></div>
</div>

<div class="row contenido-registro">
 
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <h2 style="color:#EF8200; text-align:center; font-family:Montserrat;font-weight:bold;">ÚNETE HOY</h2>
        
        <!---------------------INICIO DEL FORMULARIO-------------------->
            <div class="form"> 
                        <form action="{{route('register.store')}}" method="POST" id="formulario-principal">
                                <div class="row" style="padding-left:10px">
                                    <div class="col-12" style="margin-bottom:10px">
                                    <input type="text" name="name" class="form-control formulario" placeholder="NOMBRE" required>
                                    
                                    </div>
                                    <div class="col-12" style="margin-bottom:10px">
                                    <input type="text" name="last_name" class="form-control formulario" placeholder="APELLIDO" required>
                                    
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                    <input type="email" name="email" class="form-control formulario" placeholder="CORREO ELECTRÓNICO" required>
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                    <input type="text" name="number_document" class="form-control formulario" placeholder="NÚMERO DE DOCUMENTO" required>
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                        <input type="text" name="country" class="form-control formulario" placeholder="PAÍS">
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                        <input type="text" name="city" class="form-control formulario" placeholder="CIUDAD" required>
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                        <input type="text" name="company" class="form-control formulario" placeholder="Empresa">
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                        <input type="text" name="employment" class="form-control formulario" placeholder="Cargo">
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                    <input type="text" name="number_phone" class="form-control formulario" placeholder="TELÉFONO" required>
                                    </div>

                                    

                                    

                                    

                                    

                                    <div class="col-12" style="margin-bottom:10px; font-size:0.7rem; padding-left:10px;width:65%; color:#666666" >
                                        <label class="check-box"><input type="checkbox" id="cbox1" value="first_checkbox" style="#56497A; opacity: 0.7;" required> 
                        
                                            Acepto los términos, condiciones 
                                            y políticas de World Energy Council Colombia  
                                        </label><br>
                                    </div>
                                    

                                    <div class="col-12" style="margin-bottom:10px">
                                    @csrf
                                    <button type="submit" class="register-button">REGÍSTRATE</button>
                                </div>
                            </div>
                        </form>
                    
            </div>

            <div class="row" style="padding-top:5%; padding-bottom:5%;">
                        <div class="col-6" style="text-align:right;">
                            <h5 style="color:4d4d4d;font-family:Montserrat;font-weight:600;font-size:1.2rem;">¿Ya tienes una cuenta?</h5>
                        </div>
                        <div class="col-6">
                         <a href="{{route('login')}}" style="text-decoration:none"><h5 style="color:#EF8200; font-family:Montserrat;font-weight:600;font-size:1.2rem;">Inicia sesión</h5></a>   
                        </div>

            </div>

        <!----------------------FIN FORMULARIO--------------------------->

    </div>
    <div class="col-md-3"></div>

</div>

@endsection