@extends('layouts.master')
<style>
    .header{
        background-image: url('{{asset('imagenes/header-lobby.jpg')}}'); 
        background-size:cover;
        background-position: center;
        padding-top: 40px;
        padding-bottom: 40px; 
    }
    .btn
    {
        background: none!important;
        background-color: none;
        font-family: Montserrat;
    }
    .menu
    {
        text-align:right;
    }
    .contenido-eventos{
        padding-top:3%;
        padding-bottom: 3%;
    }
    .eventos
    {
        text-align:center;
        padding-top: 40px;
    }
    .evento-title{
       text-align:center;
       font-family:Montserrat;
       color:#4d4d4d;
        
    }
    .inscribir{
        background-color:#FFFFFF; 
        color:#0B91D6; 
        border-radius:0px; 
        border:2px solid #0B91D6; 
        font-family:Montserrat;
    }
    
</style>
@section('content')

<div class="row header">

    <div class="col-md-1"></div>
    <div class="col-md-3">
        <img src="{{ asset('imagenes/logo-blanco.png') }}" alt="logo" width="250px">
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-3 menu">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                Hola, {{auth()->user()->name}}
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a href="{{route('login.edit',auth()->user()->id)}}" class="dropdown-item" href="#" style="font-family:Montserrat">Editar perfil</a></li>
                <li><a href="{{route('login.logout')}}" class="dropdown-item" href="#"  style="font-family:Montserrat">Cerrar sesión</a></li>
                
            </ul>
        </div>
    </div>
    <div class="col-md-1"></div>

</div>

<div class="row contenido-eventos">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h2 style="color: #02314C; font-family: Montserrat, Sans-serif;font-size: 3rem;font-weight: 800;">EVENTOS</h2>
        <div class="row eventos">
            
              @foreach($eventos as $evento)
                <div class="col-md-4" style="tex-align:center">
                    <img src="{{$evento->photo}}" width="50%" alt="Event-photo">
                    <h4 style="tex-align:center; font-family:Montserrat; color:#4d4d4d">{{$evento->name_evento}}</h5>
                    <span style="tex-align:center; font-family:Montserrat; color:#EF8200">{{$evento->fecha}} <br><br></span>
                    @if(count($evento['event_user']) == 0)
                       <a href="{{route('login.eventregister',$evento->id)}}"><button class="inscribir">INSCRÍBETE AHORA</button></a> 
                        @else
                        <span style="color:#4d4d4d; font-family:Montserrat; font-weight:bold; font-size:1.2rem;">INSCRITO</span>
                        @endif
                </div>
              @endforeach  
          
        </div>
        
    </div>
    <div class="col-md-1"></div>
</div>

@endsection