@extends('layouts.master')

@section('content')
<style>

.formulario{
 
 background-color: #EDEDED;
 color:#AEADB3;
 border-radius: 0px;
 font-size: 1.2rem;
 border: none;
 font-family: Montserrat;
 padding-left: 30px;
 

}

.form2 {
    display: flex;
    align-items: center;
    margin: 0 auto;
    margin-top: 5%;

}

 .contenido-registro{
    background-image: url('{{asset('imagenes/Fondo-1.png')}}'); 
    background-size:cover;
    background-position: center;
    padding-top: 40px;
    padding-bottom: 40px;
 }  
 

 .register-button{
     width: 100%;
     background-color:#0B91D6;
     padding-top:10px;
     padding-bottom:10px;
     color:#FFFFFF;
     font-family: Montserrat;
     font-size: 1.2rem;
     font-weight: 600;
     border: none;
     border-radius: 0px;
 }
 #formulario-login{
     width: 100%;
 }

</style>

<div class="row" style="padding-top:50px; padding-bottom:50px;">
    <div class="col-md-1"></div>
    <div class="col-md-3"><img src="{{ asset('imagenes/logo-color.png') }}" alt="logo" width="250px"></div>
</div>

<div class="row contenido-registro">
 
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <h2 style="color:#EF8200; text-align:center; font-family:Montserrat;font-weight:bold;">¡BIENVENIDOS!</h2>

        <!---------------------INICIO DEL FORMULARIO-------------------->
            <div class="form2"> 
                        <form action="{{route('login.store')}}" method="POST" id="formulario-login">
                                <div class="row" style="padding-left:10px">
                                    
                                    <div class="col-12" style="margin-bottom:10px">
                                        <input type="text" name="number_document" class="form-control formulario" placeholder="NÚMERO DE DOCUMENTO" required>
                                    </div>
                                    
                                    <div class="col-12" style="margin-bottom:10px">
                                         @csrf
                                        <button type="submit" class="register-button">INICIAR SESIÓN</button>
                                    </div>
                                </div>
                        </form>
     
            </div>

            <div class="row" style="padding-top:5%; padding-bottom:5%;">
                        <div class="col-6" style="text-align:right;">
                            <h5 style="color:4d4d4d;font-family:Montserrat;font-weight:600;font-size:1.2rem;">¿Aún no tienes una cuenta?</h5>
                        </div>
                        <div class="col-6">
                           <a href="{{route('register.index')}}" style="text-decoration:none"><h5 style="color:#EF8200; font-family:Montserrat;font-weight:600;font-size:1.2rem;">Regístrate</h5></a> 
                        </div>

            </div>

        <!----------------------FIN FORMULARIO--------------------------->

    </div>
    <div class="col-md-3"></div>

</div>

@endsection