@extends('layouts.master')
<style>
    .header{
        background-image: url('{{asset('imagenes/header-lobby.jpg')}}'); 
        background-size:cover;
        background-position: center;
        padding-top: 40px;
        padding-bottom: 40px; 
    }
    .btn
    {
        background: none!important;
        background-color: none;
        font-family: Montserrat;
    }
    .menu
    {
        text-align:right;
    }
    .contenido-eventos{
        padding-top:3%;
        padding-bottom: 3%;
    }
    .eventos
    {
        text-align:center;
        padding-top: 40px;
    }
    .evento-title{
       text-align:center;
       font-family:Montserrat;
       color:#4d4d4d;
        
    }
    .inscribir{
        background-color:#EF8200; 
        color:#FFFFFF; 
        border-radius:0px; 
        border:none; 
        font-family:Montserrat;
        font-weight:bold;
        padding:10px;
    }
    .formulario{
 
        background-color: #EDEDED;
        color:#AEADB3;
        border-radius: 0px;
        font-size: 1.2rem;
        border: none;
        font-family: Montserrat;
        padding-left: 30px;
        

        }
        .register-button{
            width: 100%;
            background-color:#0B91D6;
            padding-top:20px;
            padding-bottom:20px;
            color:#FFFFFF;
            font-family: Montserrat;
            font-size: 1.2rem;
            font-weight: 600;
            border: none;
            border-radius: 0px;
        }
    
</style>
@section('content')

<div class="row header">

    <div class="col-md-1"></div>
    <div class="col-md-3">
        <img src="{{ asset('imagenes/logo-blanco.png') }}" alt="logo" width="250px">
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-3 menu">
        
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                Hola, {{auth()->user()->name}}
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a href="{{route('login.lobby')}}" class="dropdown-item" href="#" style="font-family:Montserrat">Regresar al lobby</a></li>
                
                
            </ul>
        </div>
          
    </div>
    <div class="col-md-1"></div>

</div>

<div class="row contenido-eventos">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h2 style="color: #02314C; font-family: Montserrat, Sans-serif;font-size: 3rem;font-weight: 800;">MI PERFIL</h2>
        <div class="row eventos">
            
              
                <div class="col-md-4" style="tex-align:center">                    
                   <div class="form"> 
                        <form action="{{route('login.update',$usuario)}}" method="POST" id="formulario-principal">
                            @method('put')
                                <div class="row" style="padding-left:10px">
                                    <div class="col-12" style="margin-bottom:10px">
                                    <h5 style="color:4d4d4d; font-family:Montserrat; text-align:left; font-weight:bold;"> NOMBRE:</h5>
                                        <input type="text" name="name" class="form-control formulario" value="{{$usuario->name}}" required>
                                    
                                    </div>
                                    <div class="col-12" style="margin-bottom:10px">

                                        <h5 style="color:4d4d4d; font-family:Montserrat; text-align:left; font-weight:bold;"> APELLIDO:</h5>
                                        <input type="text" name="last_name" class="form-control formulario" value="{{$usuario->last_name}}" required>
                                    
                                    </div>


                                    <div class="col-12" style="margin-bottom:10px">
                                        <h5 style="color:4d4d4d; font-family:Montserrat; text-align:left; font-weight:bold;"> TELÉFONO:</h5>
                                        <input type="text" name="number_phone" class="form-control formulario" value="{{$usuario->number_phone}}" required>
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                        <h5 style="color:4d4d4d; font-family:Montserrat; text-align:left; font-weight:bold;"> CIUDAD:</h5>
                                        <input type="text" name="city" class="form-control formulario" value="{{$usuario->city}}" required>
                                    </div>


                                    <div class="col-12" style="margin-bottom:10px">
                                    @csrf
                                    <button type="submit" class="register-button">ACTUALIZAR DATOS</button>
                                </div>
                            </div>
                        </form>
                    
            </div>
            

                </div>
               
          
        </div>
        
    </div>
    <div class="col-md-1"></div>
</div>

@endsection