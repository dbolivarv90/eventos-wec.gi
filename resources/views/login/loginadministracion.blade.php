@extends('layouts.master')

@section('content')
<style>

.formulario{
 
 background-color: #EDEDED;
 color:#AEADB3;
 border-radius: 0px;
 font-size: 1.2rem;
 border: none;
 font-family: Montserrat;
 padding-left: 30px;
 

}

.form2 {
    display: flex;
    align-items: center;
    margin: 0 auto;
    margin-top: 5%;

}

 .contenido-registro{
    background-image: url('{{asset('imagenes/Fondo-1.png')}}'); 
    background-size:cover;
    background-position: center;
    padding-top: 40px;
    padding-bottom: 40px;
 }  
 

 .register-button{
     width: 100%;
     background-color:#0B91D6;
     padding-top:10px;
     padding-bottom:10px;
     color:#FFFFFF;
     font-family: Montserrat;
     font-size: 1.2rem;
     font-weight: 600;
     border: none;
     border-radius: 0px;
 }
 #formulario-login{
     width: 100%;
 }

</style>

<div class="row" style="padding-top:50px; padding-bottom:50px;">
    <div class="col-md-1"></div>
    <div class="col-md-3"><img src="{{ asset('imagenes/logo-color.png') }}" alt="logo" width="250px"></div>
</div>

<div class="row contenido-registro">
 
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <h2 style="color:#EF8200; text-align:center; font-family:Montserrat;font-weight:bold;">¡BIENVENIDOS!</h2>

        <!---------------------INICIO DEL FORMULARIO-------------------->
            <div class="form2"> 
                        <form action="{{route('login.adminstore')}}" method="POST" id="formulario-login">
                                <div class="row" style="padding-left:10px">
                                    
                                    <div class="col-12" style="margin-bottom:10px">
                                        <input type="email" name="email" class="form-control formulario" placeholder="CORREO ELETRÓNICO" required>
                                    </div>

                                    <div class="col-12" style="margin-bottom:10px">
                                        <input type="password" name="password" class="form-control formulario" placeholder="CONTRASEÑA" required>
                                    </div>
                                    
                                    <div class="col-12" style="margin-bottom:10px">
                                         @csrf
                                        <button type="submit" class="register-button">INICIAR SESIÓN</button>
                                    </div>
                                </div>
                        </form>
     
            </div>


        <!----------------------FIN FORMULARIO--------------------------->

    </div>
    <div class="col-md-3"></div>

</div>

@endsection