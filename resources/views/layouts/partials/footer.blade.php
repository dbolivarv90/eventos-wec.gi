
<style>
    .texto-contacto{
        color:#FFFFFF;
        font-family:Montserrat;
        font-size: 0.8rem;

    }
    .texto-menu{
        color:#EF8200;
        font-family: Montserrat;
        font-size: 0.8rem;
    }
    .texto-menu2{
        color:#FFFFFF;
        font-family: Montserrat;
        font-size: 0.8rem;
    }
    @media (max-width: 768px){
        .texto-menu{
            text-align: center!important;
        }
        .texto-menu2{
            text-align: center!important;
        }
        .texto-contacto{
            text-align:center!important;
        }
        .titulo-contacto{
            text-align:center!important;
        }
        .iconos{
            text-align:center!important;
        }
    }
</style>
<div class="row" style="background-color:#02314B; padding-top:60px; padding-bottom:60px;">

    <div class="col-1"></div>
    <div class="col-md-3">
        <h3 class="titulo-contacto" style="color:#FFFFFF; font-family:Montserrat; font-weight:bold;">CONTACTO</h3>
        <p class="texto-contacto"><br><i class="fa-solid fa-phone"></i>&nbsp;&nbsp;123 456 789</p>
        <p class="texto-contacto"><i class="fa-solid fa-envelope"></i>&nbsp;&nbsp;admin@energycolombia.org</p>
        <p class="texto-contacto"><i class="fa-solid fa-location-dot"></i>&nbsp;&nbsp;Dirección</p>
    </div>
    <div class="col-md-2">
        <p class="texto-menu">
            NOSOTROS <br><br>
            MIEMBROS <br><br>
            LÍDERES ENERGÉTICOS <br><br>
            AGENDA <br><br>
            SALA DE PRENSA <br><br>
            SOSTENIBILIDAD
        </p>
    </div>

    <div class="col-md-2">
        <p class="texto-menu2">
            LORE IMPSUM <br><br>
            LORE IMPSUM <br><br>
            LORE IMPSUM <br><br>
            LORE IMPSUM <br><br>
            LORE IMPSUM <br><br>
            LORE IMPSUM
        </p>
    </div>

    <div class="col-md-3">
        <h3 class="iconos" style="color:#FFFFFF; font-family:Montserrat; font-weight:bold;">NUESTRAS <br> REDES</h3>
        <p class="iconos"> <br>
            <i class="fa-brands fa-facebook fa-lg" style="color:#EF8200; width:20px"></i>&nbsp;&nbsp;
            <i class="fa-brands fa-twitter fa-lg" style="color:#EF8200; width:20px"></i>&nbsp;&nbsp;
            <i class="fa-brands fa-instagram fa-lg" style="color:#EF8200; width:20px"></i>&nbsp;&nbsp;
            <i class="fa-brands fa-youtube fa-lg" style="color:#EF8200; width:20px"></i>&nbsp;&nbsp;
            <i class="fa-brands fa-linkedin fa-lg" style="color:#EF8200; width:20px"></i>&nbsp;&nbsp;

        </p>
        <p class="texto-contacto">
            <br>
            Políticas De Privacidad<br>
            Términos De Uso <br>
            Copyright
        </p>
    </div>
    <div class="col-1"></div>

</div>