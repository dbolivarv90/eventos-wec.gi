@extends('admin.layouts.master', [
'title' => 'Dashboard',
'breadcrumbs' => [
route('dashboard.index') => 'Dashboard'
]
])

@section('content')

<style>
    .cerrarsesion{
     width: 50%;
     background-color:#0B91D6;
     padding-top:10px;
     padding-bottom:10px;
     padding-right: 20px;
     padding-left: 20px;
     color:#FFFFFF;
     font-family: Montserrat;
     font-size: 1.2rem;
     font-weight: 600;
     border: none;
     border-radius: 0px;
     text-decoration: none;
    }

    .cerrarsesion:hover{
            background-color:#FFFFFF;
            color:#0B91D6;
            border: solid 1px;
            border-color:#0B91D6;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1>BIENVENIDO AL PANEL ADMINISTRATIVO</h1>
        </div>
    </div>
</div>


@endsection