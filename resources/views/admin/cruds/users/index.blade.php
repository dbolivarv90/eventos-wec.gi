@extends('admin.layouts.master', [
'title' => 'Usuarios',
'breadcrumbs' => [
route('dashboard.index') => 'Dashboard',
route('users.index') => 'Usuarios',
]
])

@section('content')

    @include('admin.cruds.users.partials.table-list')
    @include('admin.cruds.users.partials.modal-create')


@endsection

@section('scripts')

<script>
    $(document).ready(function () {
        var can = "";
        // console.log(can);
        var columns;
        
            columns = [ 
                { 'className': 'details-control',
                  'orderable': false,
                  'data': null,
                  'defaultContent': '',
                  render : function(data, type, row, meta){
                    return `<div id="toogleInfo">
                                <div class="btn btn-sm btn-icon btn-active-color-gray-600 ms-n3">
                                    <span class="svg-icon toggle-off svg-icon-2">
                                        <i class="fas fa-plus-square"></i>
                                    </span>
                                </div>
                            </div>`;
                }},
                { data: 'name' },
                { data: 'last_name' },
                { data: 'email', orderable: false },
                { data: 'number_document' }
            ];
        
		const table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            ajax: {
                url: '{{ route('users.index') }}',
                dataFilter: function(data){
                    var json = JSON.parse( data );
                    json.recordsTotal = json.last_page;
                    json.recordsFiltered = json.total;
        
                    return JSON.stringify( json );
                },
                data: function(data, settings){
                    const page = $('.table').DataTable().page.info().page;

                    for(let key in data.order){
                        const column = data.order[key].column;

                        data.order[key].column_name = settings.aoColumns[column].data;
                    }

                    data.page = page + 1;
                }
            },
            columns: columns,
            order: [[ 1, "asc" ]]
        });

          // Add event listener for opening and closing details
          $('.users_table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                $('#toogleInfo').html(`<div class="btn btn-sm btn-icon btn-active-color-gray-600 ms-n3">
                                            <span class="svg-icon toggle-off svg-icon-2">
                                                <i class="fas fa-plus-square"></i>
                                            </span>
                                        </div>`);
            } else {
                // Open this row
                $('#toogleInfo').html(`<div class="btn btn-sm btn-icon btn-active-color-gray-600 ms-n3">
                                            <span class="svg-icon toggle-off svg-icon-2">
                                                <i class="fas fa-minus-square"></i>
                                            </span>
                                        </div>`);
                row.child( format(row.data()) ).show();
            }
        } );

	});

    function format ( d ) {
        // `d` is the original data object for the row
        var table;
        table = `<div id="kt_create_new_payment_method_${d.id}" class="collapse show fs-7 ps-10">
                    <div class="py-5">
                        <div class="me-5">
                            <table class="table details table-flush fw-bold gy-1">
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">Nombre:</td>
                                    <td class="text-gray-800">${ d.name }</td>
                                </tr>
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">Apellido:</td>
                                    <td class="text-gray-800">${ d.last_name }</td>
                                </tr>
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">Email:</td>
                                    <td class="text-gray-800">${ d.email }</td>
                                </tr>
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">N. Documento</td>
                                    <td class="text-gray-800">${ d.number_document }</td>
                                </tr>
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">Pais:</td>
                                    <td class="text-gray-800">${ (d.country == null) ? '' : d.country }</td>
                                </tr>
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">Ciudad:</td>
                                    <td class="text-gray-800">${ (d.city == null) ? '' : d.city }</td>
                                </tr>
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">Empresa:</td>
                                    <td class="text-gray-800">${ (d.company == null) ? '' : d.company }</td>
                                </tr>
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">Cargo:</td>
                                    <td class="text-gray-800">${ (d.employment == null) ? '' : d.employment }</td>
                                </tr>
                                <tr>
                                    <td class="text-muted min-w-125px w-125px">N. Telefono:</td>
                                    <td class="text-gray-800">${ (d.number_phone == null) ? '' : d.number_phone}</td>
                                </tr>          
                            </table>
                        </div>
                    </div>
                </div>`;

        return table;
    }

</script>
@endsection