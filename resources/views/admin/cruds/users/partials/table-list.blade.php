<div class="container-fluid">
    <div class="card">
		<div class="card-body border-0 pt-6">
            <div class="pt-0 table-responsive">
                <table id="kt_datatable_example_1" class="table users_table align-middle table-row-dashed fs-6 gs-10 gy-7 gx-7">
                    <thead>
                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                            <th></th>
                            <th class="w-350px">Nombre</th>
                            <th class="w-350px">Apellido</th>
                            <th class="w-350px">Email</th>
                            <th class="w-350px">N. Documento</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>
                                <input type="text" min="1" class="form-control d-block" placeholder="Nombre">
                            </th>
                            <th>
                                <input type="text" class="form-control" placeholder="Apellido">
                            </th>
                            <th>
                                <input type="text" class="form-control" placeholder="Email">
                            </th>
                            <th>
                                <input type="text" class="form-control" placeholder="N. Documento">
                            </th>
                    </thead>
                    <tbody class="text-gray-600 fw-bold"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>