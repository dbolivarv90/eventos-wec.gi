<div class="modal fade" id="modal-create-user" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered mw-950px">
		<div class="modal-content">
			<div class="modal-header" id="kt_modal_create_api_key_header">
				<h2>Registrar Usuarios</h2>
				<div class="btn btn-sm btn-icon btn-active-color-primary modal-reset" data-bs-dismiss="modal">
					<span class="svg-icon svg-icon-1">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
								<rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
								<rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
							</g>
						</svg>
					</span>
				</div>
			</div>
			{!! Form::open(['route' => ['file-import'], 'method' => 'POST', 'files' => true, 'id' => 'users_form']) !!}
				<div class="modal-body py-10 px-lg-17">
					<div class="scroll-y me-n7 pe-7" id="kt_modal_create_api_key_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_create_api_key_header" data-kt-scroll-wrappers="#kt_modal_create_api_key_scroll" data-kt-scroll-offset="300px">
                
                        <div class="fv-row mb-7">
                            <label class="required fw-bold fs-6 mb-2">Cargar Archivo</label>
                            {!! Form::file('file', old('file'), 
                                ['required', 
                                 'id' => 'file', 
                                 'class' => 'form-control form-control-solid mb-3 mb-lg-0',
                                 'placeholder' => 'Cargar Archivo'])
                            !!}

                        </div>
                                         
					</div>
				</div>
				<div class="modal-footer flex-center">
					<button type="reset" class="btn btn-light me-3 form-modal-dismiss w-300px">Descartar</button>
					<button type="submit" id="kt_modal_create_api_key_submit" class="btn btn-info w-300px">
						<span class="indicator-label">Importar Datos</span>
						<span class="indicator-progress">Cargando...
						<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
					</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>