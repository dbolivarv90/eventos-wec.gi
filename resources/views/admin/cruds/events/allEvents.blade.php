@extends('admin.layouts.master', [
'title' => 'Todos los Eventos',
'breadcrumbs' => [
route('dashboard.index') => 'Dashboard',
route('events.index') => 'Eventos',
]
])

@section('content')

    @include('admin.cruds.events.partials.table-all-events')


@endsection

@section('scripts')

@php
$report_route = route("events.report", 'id-here');
@endphp

<script>
    $(document).ready(function () {

        var actionsHtml = `<a href="{{ $report_route }}" class="menu-link px-3 text-hover-inverse-light">
                                        <span class="mx-3"><i class="fas fa-eye"></i></span>
                                    </a>
                               `;

        var can = "";
        // console.log(can);
        var columns;
        
            columns = [ 
                { data: 'id' },
                { data: 'name_evento' },
                { data: 'actions', render: function(data, type, row, meta){
                    let actions = actionsHtml.replaceAll('id-here', row.id);

                    return `${actions}`;
                }, className: 'actions-column', orderable: false, searchable: false},
            ];
        
		const table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            ajax: {
                url: '{{ route('events.allEvents') }}',
                dataFilter: function(data){
                    var json = JSON.parse( data );
                    json.recordsTotal = json.last_page;
                    json.recordsFiltered = json.total;
        
                    return JSON.stringify( json );
                },
                data: function(data, settings){
                    const page = $('.table').DataTable().page.info().page;

                    for(let key in data.order){
                        const column = data.order[key].column;

                        data.order[key].column_name = settings.aoColumns[column].data;
                    }

                    data.page = page + 1;
                }
            },
            columns: columns,
            columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        className: 'text-end',
                        render: function (data, type, row) {
                            let actions = actionsHtml.replaceAll('id-here', row.id);

                            return `${actions}`
                        },
                    }
                ],
            order: [[ 0, "desc" ]]
        });

	});

</script>
@endsection