@extends('admin.layouts.master', [
'title' => 'Eventos',
'breadcrumbs' => [
route('dashboard.index') => 'Dashboard',
route('events.index') => 'Eventos',
route('events.create') => 'Editar',
]
])

@section('content')
<div class="container-fluid">
    <div class="mx-5 mx-xl-15 my-7">
        <div class="card">
            <div class="card-header">
				<div class="card-title fs-3 fw-bolder">Editar</div>
			</div>
            <div class="card-body border-top p-9">
                {!! Form::open(['route' => ['events.update', ['event' => $event->id]], 'method' => 'PUT', 'files' => true, 'id' => 'events_form']) !!}
                <div class="row mb-6">
                            <label class="required fw-bold fs-6 mb-2">Nombre del evento</label>
                            {!! Form::text('name_evento', $event->name_evento, 
                                ['required', 
                                 'id' => 'name_evento', 
                                 'class' => 'form-control form-control-solid mb-3 mb-lg-0',
                                 'placeholder' => 'Nombre del evento'])
                            !!}
                        </div>

                        <div class="row mb-6">
                        <label class="required fw-bold fs-6 mb-2">Fecha del evento</label>
                                {!! Form::text('fecha', old('fecha'), 
                                    ['required', 
                                     'id' => 'fecha', 
                                     'class' => 'form-control form-control-solid mb-3 mb-lg-0', 
                                     'placeholder' => 'yyyy/mm/dd'])
                                !!}
                        </div>

                        <div class="row mb-6">
                            <label class="required fw-bold fs-6 mb-2">Descripción</label>
                            {!! Form::textarea('description', $event->description,
                                ['required',
                                'id' => 'description',
                                'row' => 2,
                                'class' => 'form-control form-control-solid mb-3 mb-lg-0',
                                'placeholder' => 'Descripción'])
                            !!}
                        </div>

                        <div class="row mb-6">
                            <div class="form-check form-check-custom form-check-solid">
                                        {!! 
                                            Form::checkbox('estado_submit', null, $event->estado,
                                                ['id' => 'estado_submit',
                                                'class' => 'form-ob form-check-input'])
                                        !!}
                                        <label class="form-check-label fw-bold fs-6 mb-2" for="flexCheckDefault">
                                            Activo
                                        </label>
                            </div>
                        </div>

                        

                        <div class="row mb-6">
                            <label class="fw-bold fs-6 mb-2">Url de la imagen del evento</label>
                            {!! Form::text('photo', $event->photo, 
                                ['id' => 'photo', 
                                 'class' => 'form-control form-control-solid mb-3 mb-lg-0',
                                 'placeholder' => 'Agrega la URL donde se encuentra la foto'])
                            !!}
                        </div>
			
			</div>
            <div class="card-footer d-flex justify-content-end py-6 px-9">
                <a href="{{ route('events.index') }}" class="btn btn-light me-2">Regresar</a>
                <button type="submit" class="btn btn-info">
                    <span class="indicator-label">Actualizar</span>
                </button>
			</div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script>

    $(document).ready(function(){

        var fecha = '{{ $event->fecha}}';
        $("#fecha").daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: true,
        });
        $("#fecha").val(moment(fecha).format('YYYY/MM/DD'));
        $('#fecha').data('daterangepicker').setStartDate(moment(fecha).format("YYYY/MM/DD"));
        $('input[name="fecha"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

    });


</script>
@endsection