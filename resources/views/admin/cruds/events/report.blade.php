@extends('admin.layouts.master', [
'title' => 'Todos los Eventos',
'breadcrumbs' => [
route('dashboard.index') => 'Dashboard',
route('events.index') => 'Eventos',
]
])

@section('content')

    @include('admin.cruds.events.partials.table-report')


@endsection

@section('scripts')


<script>
    $(document).ready(function () {

        var can = "";
        // console.log(can);
        var columns;
        
            columns = [ 
                { data: 'user.id' },
                { data: 'user.name' },
                { data: 'user.last_name' },
                { data: 'user.email' },
                { data: 'user.number_document' },
                { data: 'user.country' },
                { data: 'user.city' },
                { data: 'user.company' },
                { data: 'user.employment' },
                { data: 'user.number_phone' },
            ];
        
		const table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            ajax: {
                url: '{{ route('events.report', $id) }}',
                dataFilter: function(data){
                    var json = JSON.parse( data );
                    json.recordsTotal = json.last_page;
                    json.recordsFiltered = json.total;
        
                    return JSON.stringify( json );
                },
                data: function(data, settings){
                    const page = $('.table').DataTable().page.info().page;

                    for(let key in data.order){
                        const column = data.order[key].column;

                        data.order[key].column_name = settings.aoColumns[column].data;
                    }

                    data.page = page + 1;
                }
            },
            columns: columns,
            order: [[ 0, "desc" ]]
        });

	});

</script>
@endsection