@extends('admin.layouts.master', [
'title' => 'Eventos',
'breadcrumbs' => [
route('dashboard.index') => 'Dashboard',
route('events.index') => 'Eventos',
]
])

@section('content')

    @include('admin.cruds.events.partials.table-list')
    @include('admin.cruds.events.partials.modal-create')


@endsection

@section('scripts')

@php
$edit_route = route("events.edit", ["event" => 'id-here']);
$delete_route = route("events.destroy", ["event" => 'id-here']);
@endphp

<script>
    $(document).ready(function () {
        $("#fecha").daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: true,
        });

        $('input[name="fecha"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        var actionsHtml = `
                    <div class="dropdown ">
                            <a class="w-100px btn btn-light btn-active-white btn-sm" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Acciones<i class="fas fa-angle-down"></i>
                            </a>
                            <div class="menu dropdown-menu w-50 menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 fw-bold fs-7 w-200px py-4" aria-labelledby="dropdownMenuButton1">
                                <div class="menu-item px-3">
                                    <a href="{{ $edit_route }}" class="menu-link px-3 text-hover-inverse-light">
                                        <span class="mx-3"><i class="fas fa-edit"></i></span>
                                        Editar
                                    </a>
                                </div>
                                <div class="menu-item px-3">
                                    <form class="delete-form" method="POST" action="{{ $delete_route }}">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <a href="#" class="delete-item menu-link px-3 text-hover-inverse-light">
                                            <span class="mx-3"><i class="fas fa-trash-alt"></i></span>
                                            Eliminar
                                        </a>
                                    </form>
                                </div>
                            </div>`;

        var can = "";
        // console.log(can);
        var columns;
        
            columns = [ 
                { data: 'id' }, //Get ID for show checkbox selector
                { data: 'id' }, //Return ID in the ROW
                { data: 'name_evento' },
                { data: 'description' },
                { data: 'estado_label', orderable: false },
                { data: 'photo' },
                { data: 'fecha', render: function(data){
                    return moment(data).format('D [de] MMMM YYYY');
                } },
                { data: 'actions', render: function(data, type, row, meta){
                    let actions = actionsHtml.replaceAll('id-here', row.id);

                    return `${actions}`;
                }, className: 'actions-column', orderable: false, searchable: false},
            ];
        
		const table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            ajax: {
                url: '{{ route('events.index') }}',
                dataFilter: function(data){
                    var json = JSON.parse( data );
                    json.recordsTotal = json.last_page;
                    json.recordsFiltered = json.total;
        
                    return JSON.stringify( json );
                },
                data: function(data, settings){
                    const page = $('.table').DataTable().page.info().page;

                    for(let key in data.order){
                        const column = data.order[key].column;

                        data.order[key].column_name = settings.aoColumns[column].data;
                    }

                    data.page = page + 1;
                }
            },
            columns: columns,
            columnDefs: [
                    {
		                targets: 0,
		                orderable: false,
		                render: function (data) {
		                    return `
		                        <div class="form-check form-check-sm form-check-custom form-check-solid justify-content-center">
		                            <input class="form-check-input" type="checkbox" name="events[]" value="${data}" />
		                        </div>`;
		                }
		            },
                    {
		                targets: 5,
		                render: function (data, type, row) {
                            let photo = `{{asset('/imagenes/eventDefault.png')}}`;
                            return `<img src="${row.photo ?? photo}" alt="logo" width="60" height="60">`
		                }
		            },
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        className: 'text-end',
                        render: function (data, type, row) {
                            let actions = actionsHtml.replaceAll('id-here', row.id);

                            return `${actions}`
                        },
                    }
                ],
            order: [[ 1, "desc" ]]
        });

	});

</script>
@endsection