<div class="container-fluid">
    <div class="card">
		<div class="card-body border-0 pt-6">
            <div class="pt-0 table-responsive">
                <table id="kt_datatable_example_1" class="table events_table align-middle table-row-dashed fs-6 gs-10 gy-7 gx-7">
                    <thead>
                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                            <th class="w-60px">ID</th>
                            <th class="w-1000px">Nombre</th>
                            <th class="min-w-10px">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-bold"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>