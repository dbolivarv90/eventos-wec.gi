<div class="container-fluid">
    <div class="card">
		<div class="card-body border-0 pt-6">
            <div class="pt-0 table-responsive">
                <table id="kt_datatable_example_1" class="table events_table align-middle table-row-dashed fs-6 gs-10 gy-7 gx-7">
                    <thead>
                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                            <th class="w-10px pe-2">
                                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                    <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_datatable_example_1 .form-check-input" value="1"/>
                                </div>
                            </th>
                            <th class="w-60px">ID</th>
                            <th class="w-350px">Nombre</th>
                            <th class="w-350px">Descripción</th>
                            <th class="w-350px">Estado</th>
                            <th class="w-350px">Foto</th>
                            <th class="w-350px">Fecha</th>
                            <th class="text-end min-w-100px">Acciones</th>
                        </tr>
                        <tr>
                            <th class="d-flex justify-content-center">
                                <a class="btn btn-danger my-4 p-0  d-block deleteAll" title="Eliminar múltiples Eventos">
                                    <span class="mx-3"><i class="fas fa-trash-alt"></i></span>
                                </a>
                            </th>
                            <th>
                                <input type="number" min="1" class="form-control d-block" placeholder="ID">
                            </th>
                            <th>
                                <input type="text" min="1" class="form-control d-block" placeholder="Nombre">
                            </th>
                            <th>
                                <input type="text" class="form-control" placeholder="Descripción">
                            </th>
                            <th></th>
                            <th></th>
                            <th>
                                <input type="date" class="form-control" placeholder="Fecha">
                            </th>
                            <th>
                                <button class="btn btn-info btn-reset">Limpiar</button>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-bold"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>