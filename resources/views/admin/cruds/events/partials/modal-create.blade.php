<div class="modal fade" id="modal-create-event" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered mw-950px">
		<div class="modal-content">
			<div class="modal-header" id="kt_modal_create_api_key_header">
				<h2>Agregar Nuevo</h2>
				<div class="btn btn-sm btn-icon btn-active-color-primary modal-reset" data-bs-dismiss="modal">
					<span class="svg-icon svg-icon-1">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
								<rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
								<rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
							</g>
						</svg>
					</span>
				</div>
			</div>
			{!! Form::open(['route' => ['events.store'], 'method' => 'POST', 'files' => true, 'id' => 'events_form']) !!}
				<div class="modal-body py-10 px-lg-17">
					<div class="scroll-y me-n7 pe-7" id="kt_modal_create_api_key_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_create_api_key_header" data-kt-scroll-wrappers="#kt_modal_create_api_key_scroll" data-kt-scroll-offset="300px">
                    
                        <div class="fv-row mb-7">
                            <label class="required fw-bold fs-6 mb-2">Nombre del evento</label>
                            {!! Form::text('name_evento', old('name_evento'), 
                                ['required', 
                                 'id' => 'name_evento', 
                                 'class' => 'form-control form-control-solid mb-3 mb-lg-0',
                                 'placeholder' => 'Nombre del evento'])
                            !!}
                        </div>

                        <div class="fv-row mb-7">
                        <label class="required fw-bold fs-6 mb-2">Fecha del evento</label>
                                {!! Form::text('fecha', null, 
                                    ['required', 
                                     'id' => 'fecha', 
                                     'class' => 'form-control form-control-solid mb-3 mb-lg-0', 
                                     'placeholder' => 'yyyy/mm/dd'])
                                !!}
                        </div>

                        <div class="fv-row mb-7">
                            <label class="required fw-bold fs-6 mb-2">Descripción</label>
                            {!! Form::textarea('description', old('description'),
                                ['required',
                                'id' => 'description',
                                'row' => 2,
                                'class' => 'form-control form-control-solid mb-3 mb-lg-0',
                                'placeholder' => 'Descripción'])
                            !!}
                        </div>

                        <div class="fv-row mb-7">
                            <div class="form-check form-check-custom form-check-solid">
                                        {!! 
                                            Form::checkbox('estado_submit', null, true,
                                                ['id' => 'estado_submit',
                                                'class' => 'form-ob form-check-input'])
                                        !!}
                                        <label class="form-check-label fw-bold fs-6 mb-2" for="flexCheckDefault">
                                            Activo
                                        </label>
                            </div>
                        </div>

                        

                        <div class="fv-row mb-7">
                            <label class="fw-bold fs-6 mb-2">Url de la imagen del evento</label>
                            {!! Form::text('photo', old('photo'), 
                                ['id' => 'photo', 
                                 'class' => 'form-control form-control-solid mb-3 mb-lg-0',
                                 'placeholder' => 'Agrega la URL donde se encuentra la foto'])
                            !!}
                        </div>

                                         
					</div>
				</div>
				<div class="modal-footer flex-center">
					<button type="reset" class="btn btn-light me-3 form-modal-dismiss w-300px">Descartar</button>
					<button type="submit" id="kt_modal_create_api_key_submit" class="btn btn-info w-300px">
						<span class="indicator-label">Registrar</span>
						<span class="indicator-progress">Cargando...
						<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
					</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>