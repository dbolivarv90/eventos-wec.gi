<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\{Importable, ToModel};

class UserImport implements ToModel
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = User::where('email',$row[2])->orWhere('number_document',$row[3])->first();

        if (empty($data)) {

            return new User([
                'name' => $row[0],
                'last_name' => $row[1],
                'email' => $row[2],
                'number_document' => $row[3],
                'country' => $row[4],
                'city' => ($row[5] == null) ? '' : $row[5] ,
                'company' =>  $row[6],
                'employment' =>  $row[7],
                'number_phone' =>  ($row[8] == null) ? '' : $row[8]
            ]);
        }
    }
}
