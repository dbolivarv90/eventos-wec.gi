<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\Event;
use App\Models\EventUser;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(){
      
        

        return view("login.index");

    }

    public function store(Request $request)
    {

        $user = User::where('number_document',$request->number_document)->first();

        if($user){
            Auth::login($user);
            return redirect()->route('login.lobby');
        } else {
            //return back()->withInput();
            return redirect()->route('login')->with('jsAlerterror', "Este número de documento no tiene una cuenta asociada.");
            
        }

        
    }


    public function lobby(){

        $eventos = Event::with(['event_user' => function($query) {
            $query->where('user_id', '=', auth()->user()->id );
        }])->get();
        return view("login.lobby",compact('eventos'));

    }

    public function edit($id)
    {
        $usuario = User::find($id);
        return view("login.edit",compact('usuario'));
    }

    public function update(Request $request, User $usuario)
    {
        $usuario->name = $request->name;
        $usuario->last_name = $request->last_name;
        $usuario->number_phone = $request->number_phone;
        $usuario->city = $request->city;

        $usuario->save();

        return redirect()->route('login.lobby');

    }

    public function eventregister($id)
    {
        $event_user= new EventUser();
        $event_user->event_id = $id;
        $event_user->user_id = auth()->user()->id;
        $event_user->save();

        return redirect()->route('login.lobby')->with('jsAlert',"Felicidades registro en evento procesado correctamente!");

    }

    public function logout(){

        if(auth()->user()->getRoleNames()[0] == 'Administrador')
        {
            Auth::logout();
            return redirect()->route('login.admin');
        }
        else
        {
            Auth::logout();
            return redirect()->route('login');  
        }
    }

    public function storeAdmin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            $user = Auth::user();
            return redirect()->route('dashboard.index');
           

        }

        return back()->with('jsAlerterror', "Usuario o contraseña errada");
    }

    public function admin(){
        return view('login.loginadministracion');
    }

}
