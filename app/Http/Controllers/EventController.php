<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventUser;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = Event::query();

            foreach ($request->input('order') as $order) {
                $query->orderBy($order['column_name'], $order['dir']);
            }
        
            foreach ($request->input('columns') as $key => $column) {
                if (!$request->filled("columns.$key.search.value")) continue;

                $column_name = $request->input("columns.$key.data");
                $search_value = $request->input("columns.$key.search.value");
                $date_columns = ['created_at', 'updated_at'];

                if (!in_array($column_name, $date_columns)) {
                    $query->where($column_name, 'LIKE', "%$search_value%");
                } elseif (in_array($column_name, $date_columns)) {
                    $query->whereDate($column_name, Carbon::parse($search_value)->format('Y-m-d'));
                }
            }

            $events = $query->paginate($request->length);
            $events->append(['estado_label']);

            return response()->json($events, 200);
        }

        return view('admin.cruds.events.index');
    }

    public function allEvents(Request $request)
    {
        if ($request->ajax()) {
            $query = Event::query();

            foreach ($request->input('order') as $order) {
                $query->orderBy($order['column_name'], $order['dir']);
            }

            $events = $query->paginate($request->length);

            return response()->json($events, 200);
        }

        return view('admin.cruds.events.allEvents');
    }

    public function report($id, Request $request)
    {
        if ($request->ajax()) {
            $query = EventUser::where('event_id', $id)->with(['user']);

            $events = $query->paginate($request->length);

            return response()->json($events, 200);
        }

        return view('admin.cruds.events.report', compact('id'));
    }

    
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->prepareRequest($request);

        $event = new Event();
        $event->fill($request->all());
        $event->save();

        return redirect()->route('events.index')->with('jsAlert',"Evento Creado!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::where('id', $id)->first();

        if (!$event)
            return redirect()->route('events.index')->with('jsAlerterror', "No se encontro el evento");

        return view('admin.cruds.events.edit', compact('event'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);

        if (!$event)
            return redirect()->route('event.index')->with('jsAlerterror', "No se encontro el evento");

        $request = $this->prepareRequest($request);
        $event->fill($request->all());
        $event->save();

    
        return redirect()->route('events.index')->with('jsAlert',"Evento Actualizado!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        if (!$event)
            return redirect()->route('events.index')->with('jsAlerterror', "No se encontro el evento");

        $event->event_user()->delete();
        $event->delete();

        return redirect()->route('events.index')->with('jsAlert',"Evento Eliminado!");
    }

    private function prepareRequest(Request $request){

        $estado = ($request->input('estado_submit') == "on") ? 1 : 0;

        $request->request->add(['estado' => $estado]);

        return $request;
    }

    public function deleteEvent(Request $request)
    {
        try{
            $events = Event::whereIn('id', $request->ids)->get();
            
            foreach($events as $event){
                $delete_event = Event::findOrFail($event->id);
                $delete_event->event_user()->delete();
                $delete_event->delete();
            }

            $message = array('message' => 'Eventos eliminados exitosamente', 'title' => 'Columnas Eliminadas');
            return response()->json($message);

        } catch(\Exception $e){
            //some Code 
        }
    }
}
