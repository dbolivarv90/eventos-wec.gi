<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
class RegisterController extends Controller
{
    public function index(){
      
        
        $message = "";
        return view("register.index",compact('message'));

    }

    
    public function store(Request $request)
    {
        $rules = ['email'   => 'unique:users', 'number_document'=> 'unique:users'];

        $customErrorMessages = ['email.unique' => 'Este Correo Electronico ya fue registrado anteriormente', 
        'number_document'=>'Este número de documento ya fue registrado anteriormente'];

        $validator = Validator::make($request->all(), $rules, $customErrorMessages);

        $message = ""; 
        if ($validator->fails()) {
            
            $errors = [];

            foreach($validator->errors()->toArray() as $error){
                $message = $error[0];
                return redirect()->route('register.index')->with('jsAlerterror', "¡ya existe una cuenta con este correo o este número de documento!");
            }
        }

        $user= new User();
        $user->fill($request->all());
        $user->save();

        $user->assignRole('Cliente');

        return redirect()->route('register.index')->with('jsAlert', "¡Registro exitoso!");

       //Envio de Correo
        //$to_email = $request->email;

        //Mail::to($to_email)->send(new RegisterEmail);       
    
    }



}
