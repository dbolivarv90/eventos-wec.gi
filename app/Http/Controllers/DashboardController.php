<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){

        if(auth()->user()->getRoleNames()[0] == 'Administrador')
            return view("admin.index");
        else
            return view('admin.error');
      
        

    }
}
