<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\EventUser;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_evento',
        'fecha',
        'description',
        'estado',
        'photo',
        
    ];

    public function event_user()
    {
        return $this->hasMany(EventUser::class, 'event_id', 'id');
    }

    public function getEstadoLabelAttribute()
    {
        $estado = $this->estado === 1 ? 
                ['class' => 'success', 'label' => 'Activo'] : 
                ['class' => 'warning', 'label' => 'Inactivo'] ;

        return '<div class="badge badge-light-' . $estado['class'] . ' fs-8 fw-bolder">
            ' . $estado['label'] . '
        </div>';
    }
}
